
package br.com.desafioconcrete.data;

import com.google.gson.annotations.SerializedName;

public class User {

    public String login;
    public int id;
    @SerializedName("avatar_url")
    public String avatarUrl;

}
