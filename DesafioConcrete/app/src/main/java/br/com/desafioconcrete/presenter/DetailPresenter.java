package br.com.desafioconcrete.presenter;

import java.util.List;

import javax.inject.Inject;

import br.com.desafioconcrete.DesafioApp;
import br.com.desafioconcrete.communication.CustomCallback;
import br.com.desafioconcrete.communication.GithubService;
import br.com.desafioconcrete.data.RepoDetail;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class DetailPresenter {

    private final DetailView mView;
    private final String repo;
    private final String creator;

    @Inject
    GithubService mService;

    public DetailPresenter(DetailView view, String repo, String creator) {
        this.mView = view;
        this.repo = repo;
        this.creator = creator;
        injectDependencies();
    }

    private void injectDependencies() {
        if (DesafioApp.getInstance() != null) {
            DesafioApp.getInstance().getComponent().inject(this);
        }
    }

    public void getDetail() {
        mView.showLoading(true);
        doRequest();
    }

    void doRequest() {
        mService.getPullRequests(creator, repo).enqueue(new CustomCallback<List<RepoDetail>>() {
            @Override
            public void onSuccess(List<RepoDetail> body) {
                onDetailReceived(body);
            }

            @Override
            public void onError() {
                onErrorReceived();
            }
        });
    }

    void onErrorReceived() {
        mView.showError();
    }


    void onDetailReceived(List<RepoDetail> list) {
        mView.showLoading(false);
        mView.onDetailReceived(list);
    }

    public interface DetailView {
        void showLoading(boolean show);

        void onDetailReceived(List<RepoDetail> list);

        void showError();
    }
}
