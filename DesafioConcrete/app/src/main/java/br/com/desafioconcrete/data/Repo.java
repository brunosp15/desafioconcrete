
package br.com.desafioconcrete.data;

public class Repo {

    public int id;
    public String name;
    public String fullName;
    public User owner;
    public String description;
    public int forks;
    public int watchers;

}
