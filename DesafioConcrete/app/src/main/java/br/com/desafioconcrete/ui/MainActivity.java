package br.com.desafioconcrete.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.adapter.RepoAdapter;
import br.com.desafioconcrete.data.Repo;
import br.com.desafioconcrete.presenter.MainPresenter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class MainActivity extends AppCompatActivity implements MainPresenter.MainView, RepoAdapter.OnRepoClickListener {

    private MainPresenter mPresenter;
    private RepoAdapter mAdapter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.main_list)
    EndlessList mList;

    @BindView(R.id.main_loading)
    View mLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Github JavaPop");
        mList.setLayoutManager(new LinearLayoutManager(this));
        mList.setEndlessListener(new EndlessList.EndlessRecyclerListener() {
            @Override
            public void onReachEnd(int toPage) {
                mPresenter.getPage(toPage);
            }
        });
        mPresenter = new MainPresenter(this);
        mPresenter.getPage(0);
    }

    @Override
    public void onListReceived(List<Repo> repoList) {
        if (mAdapter == null) {
            mAdapter = new RepoAdapter(repoList, this);
            mList.setAdapter(mAdapter);
        } else {
            mAdapter.addAll(repoList);
        }
    }

    @Override
    public void showLoading(boolean show) {
        if (show) {
            mLoading.setVisibility(View.VISIBLE);
        } else {
            mLoading.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError() {
        Snackbar.make(mList, "Ocorreu um erro com a sua requisição", Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.retry();
            }
        });
    }

    @Override
    public void onRepoClick(Repo repo) {
        Intent intent = DetailActivity.newIntent(this, repo.owner.login, repo.name);
        startActivity(intent);
    }
}
