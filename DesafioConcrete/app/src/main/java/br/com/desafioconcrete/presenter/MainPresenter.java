package br.com.desafioconcrete.presenter;

import java.util.List;

import javax.inject.Inject;

import br.com.desafioconcrete.DesafioApp;
import br.com.desafioconcrete.communication.CustomCallback;
import br.com.desafioconcrete.communication.GithubService;
import br.com.desafioconcrete.data.ListResponse;
import br.com.desafioconcrete.data.Repo;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class MainPresenter {

    private final MainView mView;
    private int mLastPageRequested;

    @Inject
    GithubService mService;

    public MainPresenter(MainView view) {
        mView = view;
        injectDependencies();
    }

    void injectDependencies() {
        if (DesafioApp.getInstance() != null) {
            DesafioApp.getInstance().getComponent().inject(this);
        }
    }

    public void getPage(int page) {
        mView.showLoading(true);
        setLatRequestedPage(page);
        doRequest(page);
    }

    void doRequest(int page) {
        mService.getRepositories("language:Java", "stars", page).enqueue(new CustomCallback<ListResponse>() {
            @Override
            public void onSuccess(ListResponse body) {
                onListReceived(body);
            }

            @Override
            public void onError() {
                onListError();
            }
        });
    }

    void onListError() {
        mView.showError();
    }

    void onListReceived(ListResponse body) {
        mView.showLoading(false);
        if (body != null && body.items != null && body.items.size() > 0) {
            mView.onListReceived(body.items);
        }
    }

    public void retry() {
        getPage(mLastPageRequested);
    }

    void setLatRequestedPage(int page) {
        mLastPageRequested = page;
    }


    public interface MainView {
        void onListReceived(List<Repo> repoList);

        void showLoading(boolean show);

        void showError();
    }
}
