package br.com.desafioconcrete.communication;

import java.util.List;

import br.com.desafioconcrete.data.ListResponse;
import br.com.desafioconcrete.data.RepoDetail;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by bpinto2 on 11/16/16.
 */

public interface GithubService {

    @GET("/search/repositories")
    Call<ListResponse> getRepositories(@Query("q") String language,
                                       @Query("sort") String sort,
                                       @Query("page") int page);

    @GET("/repos/{creator}/{repo}/pulls")
    Call<List<RepoDetail>> getPullRequests(@Path("creator") String creator,
                                           @Path("repo") String repo);

}
