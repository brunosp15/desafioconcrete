package br.com.desafioconcrete.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.data.Repo;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.RepoViewHolder> {

    private List<Repo> mList;
    private OnRepoClickListener mCallback;

    public RepoAdapter(List<Repo> list, OnRepoClickListener listener) {
        mList = list;
        mCallback = listener;
    }

    public void addAll(List<Repo> list) {
        int oldSize = mList.size();
        mList.addAll(list);
        notifyItemRangeInserted(oldSize, mList.size());
    }

    @Override
    public RepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo, parent, false);
        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepoViewHolder holder, int position) {
        Repo repo = mList.get(position);
        holder.bind(repo);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class RepoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.repo_name)
        TextView name;

        @BindView(R.id.repo_description)
        TextView description;

        @BindView(R.id.repo_forks)
        TextView forks;

        @BindView(R.id.repo_stars)
        TextView stars;

        @BindView(R.id.repo_username)
        TextView username;

        @BindView(R.id.repo_user_img)
        ImageView photo;

        public RepoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Repo repo) {
            name.setText(repo.name);
            description.setText(repo.description);
            forks.setText(String.valueOf(repo.forks));
            stars.setText(String.valueOf(repo.watchers));
            username.setText(repo.owner.login);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onRepoClick(repo);
                }
            });

            Picasso.with(name.getContext())
                    .load(repo.owner.avatarUrl)
                    .placeholder(R.drawable.ic_account)
                    .into(photo);
        }
    }

    public interface OnRepoClickListener {
        void onRepoClick(Repo repo);
    }
}
