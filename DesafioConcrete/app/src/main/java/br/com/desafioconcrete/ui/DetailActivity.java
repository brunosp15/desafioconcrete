package br.com.desafioconcrete.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.adapter.RepoDetailAdapter;
import br.com.desafioconcrete.data.RepoDetail;
import br.com.desafioconcrete.presenter.DetailPresenter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class DetailActivity extends AppCompatActivity implements DetailPresenter.DetailView, RepoDetailAdapter.OnDetailClickListener {

    private static final String KEY_CREATOR = "key_creator";
    private static final String KEY_REPO = "key_repo";
    private DetailPresenter mPresenter;
    private RepoDetailAdapter mAdapter;

    @BindView(R.id.detail_list)
    RecyclerView mList;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.detail_loading)
    View mLoading;


    public static Intent newIntent(Context context, String creator, String repo) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(KEY_CREATOR, creator);
        intent.putExtra(KEY_REPO, repo);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = getIntent();
        String creator = intent.getStringExtra(KEY_CREATOR);
        String repo = intent.getStringExtra(KEY_REPO);
        if (creator == null || repo == null) {
            throw new IllegalArgumentException("Creator and Repository not found");
        }
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(repo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mList.setLayoutManager(new LinearLayoutManager(this));
        mPresenter = new DetailPresenter(this, repo, creator);
        mPresenter.getDetail();

    }


    @Override
    public void showLoading(boolean show) {
        if (show) {
            mLoading.setVisibility(View.VISIBLE);
        } else {
            mLoading.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDetailReceived(List<RepoDetail> list) {
        mAdapter = new RepoDetailAdapter(this, list);
        mList.setAdapter(mAdapter);
    }

    @Override
    public void showError() {
        Snackbar.make(mList, "Ocorreu um erro com a sua requisição", Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.getDetail();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetailClick(RepoDetail repoDetail) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(repoDetail.url));
        startActivity(i);
    }
}
