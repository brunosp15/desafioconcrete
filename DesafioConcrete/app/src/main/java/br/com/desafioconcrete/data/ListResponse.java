
package br.com.desafioconcrete.data;

import java.util.ArrayList;
import java.util.List;

public class ListResponse {

    public int totalCount;
    public boolean incompleteResults;
    public List<Repo> items = new ArrayList<Repo>();

}
