
package br.com.desafioconcrete.data;

public class RepoDetail {

    public int id;
    public String title;
    public User user;
    public String body;
    public String createdAt;
    public String updatedAt;
    public String state;
    public String url;

}
