package br.com.desafioconcrete.dagger;

import android.content.Context;

import javax.inject.Singleton;

import br.com.desafioconcrete.DesafioApp;
import dagger.Module;
import dagger.Provides;

/**
 * Created by bpinto2 on 11/16/16.
 */
@Module
public class ApplicationModule {

    private DesafioApp mApplication;

    public ApplicationModule(DesafioApp application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return mApplication;
    }
}

