package br.com.desafioconcrete.ui;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class EndlessList extends RecyclerView {

    private EndlessRecyclerOnScrollListener mScrollListener;

    public EndlessList(Context context) {
        this(context, null);
    }

    public EndlessList(Context context, AttributeSet attrs) {
        super(context, attrs);
        super.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

    /**
     * IGNORED! To use the endless behavior the layout manager should be {@link android.support.v7.widget.LinearLayoutManager}
     * and its set automatically
     */
    @Override
    public void setLayoutManager(LayoutManager layout) {
        return;
    }

    /**
     * Set the listener to be notified when the scroll reach to end of screen
     *
     * @param listener
     */
    public void setEndlessListener(EndlessRecyclerListener listener) {
        setEndlessListener(2, listener);
    }

    public void setEndlessListener(int visibleThreshold, EndlessRecyclerListener listener) {
        addOnScrollListener(mScrollListener = new EndlessRecyclerOnScrollListener(
                (LinearLayoutManager) getLayoutManager(), listener, visibleThreshold));
    }

    /**
     * Set pagination to initial state
     */
    public void resetPagination() {
        mScrollListener.resetPagination();
    }

    private class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

        private int previousTotal = 0; // The total number of repos in the dataset after the last load
        private boolean loading = true; // True if we are still waiting for the last set of data to load.
        private int visibleThreshold; // Minimum amount of repos to have below current scroll pos before load more.
        private int firstVisibleItem, visibleItemCount, totalItemCount;
        private int currentPage;

        private LinearLayoutManager mLinearLayoutManager;
        private EndlessRecyclerListener endlessListener;

        EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager,
                                        EndlessRecyclerListener endlessListener, int visibleThreshold) {
            this.mLinearLayoutManager = linearLayoutManager;
            this.endlessListener = endlessListener;
            this.visibleThreshold = visibleThreshold;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            visibleItemCount = recyclerView.getChildCount();
            totalItemCount = mLinearLayoutManager.getItemCount();
            firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)) {

                if (endlessListener != null) {
                    endlessListener.onReachEnd(++currentPage);
                }

                loading = true;
            }
        }

        void resetPagination() {
            loading = true;
            previousTotal = firstVisibleItem = visibleItemCount = totalItemCount = currentPage = 0;
        }
    }

    public interface EndlessRecyclerListener {
        void onReachEnd(int toPage);
    }

    /* Bug fix to canScrollVertically returning false prematurely */
    @Override
    public boolean canScrollVertically(int direction) {
        if (direction < 1) {
            boolean original = super.canScrollVertically(direction);
            return !original && getChildAt(0) != null && getChildAt(0).getTop() < 0 || original;
        }
        return super.canScrollVertically(direction);

    }
}