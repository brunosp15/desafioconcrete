package br.com.desafioconcrete.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.data.RepoDetail;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class RepoDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER = 0;
    private static final int ITEM = 1;

    private List<RepoDetail> mList;
    private int mOpenedCount;
    private int mClosedCount;
    private OnDetailClickListener mCallback;

    public RepoDetailAdapter(OnDetailClickListener listener, List<RepoDetail> list) {
        mCallback = listener;
        mList = list;
        for (RepoDetail repoDetail : mList) {
            if (repoDetail.state.equals("open")) {
                mOpenedCount++;
            } else {
                mClosedCount++;
            }
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detail_header, parent, false);
            return new RepoDetailHeaderViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detail, parent, false);
            return new RepoDetailItemViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RepoDetailItemViewHolder) {
            RepoDetail repo = mList.get(position - 1);
            ((RepoDetailItemViewHolder) holder).bind(repo);
        } else {
            ((RepoDetailHeaderViewHolder) holder).bind(mOpenedCount, mClosedCount);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER;
        } else {
            return ITEM;
        }
    }

    public class RepoDetailItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.repo_detail_title)
        TextView title;

        @BindView(R.id.repo_detail_body)
        TextView body;

        @BindView(R.id.repo_detail_username)
        TextView username;

        @BindView(R.id.repo_detail_user_img)
        ImageView photo;

        public RepoDetailItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final RepoDetail repo) {
            title.setText(repo.title);
            body.setText(repo.body);
            username.setText(repo.user.login);
            Picasso.with(title.getContext()).load(repo.user.avatarUrl).placeholder(R.drawable.ic_account).into(photo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onDetailClick(repo);
                }
            });
        }
    }

    public class RepoDetailHeaderViewHolder extends RecyclerView.ViewHolder {

        public RepoDetailHeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int opened, int closed) {
            ((TextView) itemView).setText(String.format("%d opened / %d closed", opened, closed));
        }
    }

    public interface OnDetailClickListener {
        void onDetailClick(RepoDetail repoDetail);
    }
}
