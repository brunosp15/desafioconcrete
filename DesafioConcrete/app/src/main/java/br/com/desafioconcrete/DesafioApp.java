package br.com.desafioconcrete;

import android.app.Application;

import br.com.desafioconcrete.dagger.ApplicationComponent;
import br.com.desafioconcrete.dagger.ApplicationModule;
import br.com.desafioconcrete.dagger.DaggerApplicationComponent;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class DesafioApp extends Application {

    private static DesafioApp sInstance;
    private ApplicationComponent mComponent;

    public static synchronized DesafioApp getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        setupModules();
    }

    private void setupModules() {
        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }
}
