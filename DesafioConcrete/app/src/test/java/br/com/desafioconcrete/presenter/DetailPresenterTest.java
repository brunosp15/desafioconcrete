package br.com.desafioconcrete.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class DetailPresenterTest {

    @Mock
    DetailPresenter.DetailView mView;

    DetailPresenter mPresenter;

    @Before
    public void setup() {
        initMocks(this);
        mPresenter = spy(new DetailPresenter(mView, "repo", "creator"));
        doNothing().when(mPresenter).doRequest();
    }

    @Test
    public void when_getDetail_should_show_loading() {
        mPresenter.getDetail();

        verify(mView, times(1)).showLoading(eq(true));
    }

    @Test
    public void when_getDetail_should_doRequest() {
        mPresenter.getDetail();

        verify(mPresenter, times(1)).doRequest();
    }

    @Test
    public void when_onErrorReceived_should_call_showError() {
        mPresenter.onErrorReceived();

        verify(mView, times(1)).showError();
    }

    @Test
    public void when_onDetailReceived_should_hide_loading() {
        mPresenter.onDetailReceived(anyList());

        verify(mView, times(1)).showLoading(eq(false));
    }

    @Test
    public void when_onDetailReceived_should_call_onDetailReceived_on_view() {
        mPresenter.onDetailReceived(anyList());

        verify(mView, times(1)).onDetailReceived(anyList());
    }


}
