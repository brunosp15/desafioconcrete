package br.com.desafioconcrete.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;

import br.com.desafioconcrete.data.ListResponse;
import br.com.desafioconcrete.data.Repo;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by bpinto2 on 11/16/16.
 */

public class MainPresenterTest {

    @Mock
    MainPresenter.MainView mView;

    MainPresenter mPresenter;

    @Before
    public void setup() {
        initMocks(this);
        mPresenter = spy(new MainPresenter(mView));
        doNothing().when(mPresenter).doRequest(anyInt());
    }


    @Test
    public void when_getPage_should_show_loading() {
        mPresenter.getPage(0);

        verify(mView, times(1)).showLoading(eq(true));
    }

    @Test
    public void when_getPage_should_store_last_page() {
        mPresenter.getPage(0);

        verify(mPresenter, times(1)).setLatRequestedPage(eq(0));
    }

    @Test
    public void when_getPage_should_call_doRequest() {
        mPresenter.getPage(0);

        verify(mPresenter, times(1)).doRequest(eq(0));
    }

    @Test
    public void when_onListError_should_call_onError() {
        mPresenter.onListError();

        verify(mView, times(1)).showError();
    }

    @Test
    public void when_onListReceived_should_hide_loading() {
        mPresenter.onListReceived(any(ListResponse.class));

        verify(mView, times(1)).showLoading(eq(false));
    }

    @Test
    public void when_onListReceived_with_empty_body_do_nothing() {
        mPresenter.onListReceived(any(ListResponse.class));

        verify(mView, times(0)).onListReceived(anyList());
    }

    @Test
    public void when_onListReceived_with_NOT_empty_body_should_show_list() {
        ListResponse listResponse = new ListResponse();
        listResponse.items = new ArrayList<>(1);
        listResponse.items.add(new Repo());
        mPresenter.onListReceived(listResponse);

        verify(mView, times(1)).onListReceived(anyList());
    }

    @Test
    public void when_retry_should_call_getPage_with_the_last_page_requested() {
        mPresenter.setLatRequestedPage(10);

        mPresenter.retry();

        verify(mPresenter, times(1)).getPage(eq(10));
    }
}
